require('dotenv').config();
const request = require('request');
const schedule = require('node-schedule');
const moment = require('moment-timezone');
const jmoment = require('moment-jalaali');

let url = process.env.TELEGRAM_URL;

const sendMessage = (message) => {
	let msg = 'Testing this...';
	if(message) {
		msg = message
	}
	request.post(url, {body: {
		recipient:  '@qmetabasechannel',
		msg:		msg,
		options:	{
			parse_mode: "HTML",
		},
	}, timeout: 10000, json:true}, function (error, response, body){
		if(response && response.statusCode){

		}else{
			console.error('send telegram from iran', error, response, body);
		}
	});
}


const sql = require('mssql')
const sqlConfig = {
  user: 	process.env.DB_USER,
  password: process.env.DB_PASSWORD,
  database: process.env.DB_NAME,
  server: 	process.env.DB_SERVER,
  port: 	+process.env.DB_PORT,
  pool: {
    max: 10,
    min: 0,
    idleTimeoutMillis: 30000
  },
  options: {
    encrypt: false,
    trustServerCertificate: true
  },
  requestTimeout: 120000,
  connectionTimeout: 120000,
};

let pool;

(async () => {
 try {
  // make sure that any items are correctly URL encoded in the connection string
  if(sqlConfig.password)
  	pool = await sql.connect(sqlConfig)

 } catch (err) {
	 console.log('database errrorrr', err)
 }
})();


const query = sql => {
	return new Promise(async resolve => {
		let result = await pool.request().query(sql);
		return resolve(result.recordset);
	})
}

const doCalc1 = async () => {
	let result = await query(`SELECT 'All subscription' as 'name', count(distinct msisdn) as 'count' FROM dbo.Subscriptions WHERE status = 2`);
	const count = result?.[0]?.count ?? 0;
	
	return count;
}

const doCalc2 = async () => {
	let result = await query(`SELECT 'All subscription' as 'name', count(distinct msisdn) as 'count' FROM dbo.Subscriptions WHERE status = 2`);
	const count = result?.[0]?.count ?? 0;
	
	return count;
}

const doCalcWeekday = async (date) => {
	let result = await query(`select 

    count(*) as 'count'

from dbo.subscriptions

where effectiveamount > 0
and createdat >= DATEADD(day, -10, getdate())
and format(cast(createdat as date),'yyyy/MM/dd','fa') = '${date}'
`);
	const count = result?.[0]?.count ?? 0;
	
	return count;
}

const sendCalcs = async () => {
	const [count1, count2] = await Promise.all([doCalc1(), doCalc2()]);
	
	sendMessage(`The count is: ${count1} and ${count2}`);
}

const sendWeekdays = async () => {
	const thisweekdays = thisWeekDays();
	let counts = thisweekdays.map(weekday => doCalcWeekday(weekday.date));
	counts = await Promise.all(counts);

	thisweekdays.forEach( (value, i) => {
		value.count = counts[i];
	});

	let message = thisweekdays.map(weekday => `${weekday.day}: ${weekday.count}`).join('\n');
	
	sendMessage(message);
}

/////////

const weekdays = [
	'یکشنبه',
	'دوشنبه',
	'سه‌شنبه',
	'چهارشنبه',
	'پنجشنبه',
	'جمعه',
	'شنبه',
];

const thisWeekDays = () => {
	const weekday = moment().weekday();
	const offset = 2;

	let days = weekday + offset;
	if(days > 7) days -= 7;
	
	const daysInfo = [];

	for(let i = 0; i < days; i++) {
		const day = jmoment().subtract(i, 'days');
		daysInfo.unshift({
			day: weekdays[day.weekday()],
			date: day.format('jYYYY/jMM/jDD')
		});
	}

	return daysInfo;
}

setTimeout(async () => {
	sendCalcs();
	sendWeekdays();
}, 3000)

let rule = new schedule.RecurrenceRule();

// your timezone
rule.tz = 'Asia/Tehran';

// runs at 22:36:00
rule.second = 0;
rule.minute = 50;
rule.hour = 22;

// schedule
schedule.scheduleJob(rule, function () {
	sendCalcs();
	sendWeekdays();
});
